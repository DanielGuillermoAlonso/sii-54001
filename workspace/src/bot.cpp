#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main()
{
    DatosMemCompartida* puntero1;
    int fd;
    struct stat bstat;
    void* p;
    
    if ((fd=open("/tmp/mem.dat",O_RDWR,0666))<0){
           perror("No puede abrirse mem.dat en bot.cpp");
           return 1;
    }
    if (fstat(fd,&bstat)<0){
           perror("Error en el fstat del archivo del bot.cpp");
           close(fd);
    }
    if ((p=mmap(NULL,bstat.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0))==(void*)MAP_FAILED) {
           perror("Error de proyeccion de memoria en bot.cpp");
           close(fd);
           return 1;
    }
    close(fd);
    puntero1=(DatosMemCompartida*)p;
    
    while(1)
    {
        if ((puntero1->esfera.centro.y)>((puntero1->raqueta1.y1+puntero1->raqueta1.y2)/2))
           puntero1->accion=1;
        else if ((puntero1->esfera.centro.y)<((puntero1->raqueta1.y1+puntero1->raqueta1.y2)/2))
           puntero1->accion=-1;
        else puntero1->accion=0;
        usleep(25000);
    }

}

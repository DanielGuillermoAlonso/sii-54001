#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include "Puntos.h"

int main (){
      int fd;
      Puntos p;
      unlink("/tmp/FIFO"); //Se cierra la tubería si previamente se ha quedado abierta
      if ((mkfifo("/tmp/FIFO",0600))<0) {
          perror("No puede crearse el FIFO");
          return 1;
      }
      if ((fd=open("/tmp/FIFO",O_RDONLY))<0){
          perror("No puede abrirse el FIFO");
          return 1;
      }

      while (read(fd,&p,sizeof(p))==sizeof(p)){ 
          if (p.jugador==1){
           printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",p.pjugador1);
          }
          if (p.jugador==2){
           printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",p.pjugador2);
          }
          if (p.jugador==3){
               if (p.pjugador1>p.pjugador2)
               {
                printf("Jugador1 ha ganado\n");
               }
               if (p.pjugador1<p.pjugador2)
               {
                printf("Jugador2 ha ganado\n");
               }
           close(fd);
           unlink("/tmp/FIFO");
           return 0;
         } 
      }
}

// Mundo.cpp: implementation of the CMundo class.
//
// Autor: Daniel Guillermo Alonso
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>
#include <signal.h>
#include "Puntos.h"

#define MAXPUNTUACION 3 //Límite de puntos por jugador

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MundoServidor::MundoServidor()
{
	Init();
}

MundoServidor::~MundoServidor()
{
       close(fd);
       unlink("/tmp/FIFO");
}
void* hilo_comandos(void* d)
{
      MundoServidor* p=(MundoServidor*) d;
      p->RecibeComandosJugador();
}

void MundoServidor::RecibeComandosJugador()
{
	
     while (1) {
            usleep(10);
            char cad_cs[100];
            com.Receive(cad_cs,sizeof(cad_cs));
            unsigned char key;
            sscanf(cad_cs,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void MundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void MundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"%s: %d",nombre_jugador,puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoServidor::OnTimer(int value)
{	
        Puntos p;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Disminuye(); //La esfera disminuye con el tiempo
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		esfera.setRadio(1.0f); //Se devuelve a la esfera a su tamaño original tras anotar un punto uno de los jugadores
		puntos2++;
		p.pjugador1=puntos1;
		p.pjugador2=puntos2;
		p.jugador=2;
		write(fd,&p,sizeof(p));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.setRadio(1.0f); //Se devuelve a la esfera a su tamaño original tras anotar un punto uno de los jugadores
		puntos1++;
		p.pjugador1=puntos1;
		p.pjugador2=puntos2;
		p.jugador=1;
		write(fd,&p,sizeof(p));
	}
	
	if (puntos1==MAXPUNTUACION || puntos2==MAXPUNTUACION) //Se ha llegado al límite de puntuación
	{
	       p.jugador=3; 
	       write(fd,&p,sizeof(p));
	}
        sprintf(cad_sc,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
        //Envío de coordenadas del servidor al cliente por socket
        com.Send(cad_sc,sizeof(cad_sc));
        
}

void MundoServidor::OnKeyboardDown(unsigned char key, int x, int y){}

void capturar(int s){
        printf("Capturada la señal %s\n",strsignal(s));
        exit(s);
}

void capturar_sigusr2(int s){
        printf("Capturada la señal SIGUSR2\n");
        exit(0);
}

void MundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	//Apertura de la tubería
	if((fd=open("/tmp/FIFO",O_WRONLY))<0){
	       perror("No puede abrirse el FIFO");
	}
	
	//Creación del thread
	if (pthread_create(&thid1, NULL, hilo_comandos, this)) {
		perror("pthread_create");
	}
	
	//Captura de señales
	
	struct sigaction acc1;
	acc1.sa_handler=capturar;
	acc1.sa_flags=SA_RESTART;
	sigemptyset(&acc1.sa_mask);
	sigaction(SIGINT,&acc1,NULL);
	sigaction(SIGTERM,&acc1,NULL);
	sigaction(SIGPIPE,&acc1,NULL);
	
	struct sigaction acc2;
	acc2.sa_handler=capturar_sigusr2;
	acc2.sa_flags=SA_RESTART;
	sigaction(SIGUSR2,&acc2,NULL);
	
	//Conexión socket
	conex.InitServer("127.0.0.1",10000);
	com=conex.Accept();
	com.Receive(nombre_jugador,sizeof(nombre_jugador));
	printf("Entra el jugador %s.\n",nombre_jugador);
	
	
}

# Changelog

## [v5.0] - 9-1-2022
### Añadido
- Rama de la práctica 5.
- Los ficheros Socket.cpp y Socket.h en la carpeta workspace.

### Cambiado
- Los ficheros MundoCliente.cpp y MundoServidor.cpp con sus cabeceras, para que el servidor y el cliente se comuniquen por sockets en vez de por tuberías.
- El fichero CMakeLists.txt debido a la incorporación de Socket.cpp.

## [v4.0] - 8-12-2021
### Añadido
- Rama de la práctica 4.
- Los ficheros MundoCliente.cpp y MundoServidor.cpp con sus respectivas cabeceras, desarrollados a partir de Mundo.cpp y Mundo.h.
- Los ficheros servidor.cpp y cliente.cpp, desarrollados a partir de tenis.cpp.
- Las modificaciones necesarias en el cliente y servidor para que se produzca el envío de las coordenadas del servidor al cliente y el envío de las teclas pulsadas del cliente al servidor.
- El ejercicio opcional propuesto que consiste en incorporar en el servidor la captura de las señales SIGINT, SIGTERM, SIGPIPE y SIGUSR2 para que saquen por la salida estándar el estado de terminación del proceso.

### Cambiado
- El fichero CMakeLists.txt para generar los ejecutables servidor y cliente.

### Eliminado
- El fichero Mundo.cpp.
- El fichero Mundo.h.
- El fichero tenis.cpp.

## [v3.0] - 29-11-2021
### Añadido
- Rama de la práctica 3.
- El programa logger.cpp que, por medio de una tubería con nombre, recibe del juego el nombre y número de puntos del jugador cada vez que anota un tanto y lo imprime en pantalla.
- El programa bot.cpp que, por medio del mecanismo de ficheros proyectados en memoria, intercambia la información necesaria con el juego tenis para que el bot controle la raqueta1.
- La funcionalidad extra, que consiste en que el juego finalice cuando uno de los jugadores alcance los 3 puntos.

### Cambiado
- Los programas Mundo.h y Mundo.cpp para que el juego pueda comunicarse con el bot y el logger.

## [v2.0] - 15-11-2021
### Añadido
- Rama de la práctica 2.
- Movimiento de raqueta y esfera en el juego de tenis.
- Funcionalidad extra del juego que consiste en disminuir el tamaño de la esfera a medida que avanza el juego. La esfera vuelve a su tamaño original cuando uno de los jugadores anote un punto.
- El fichero README.md, que contiene los controles del juego.

## [v1.3] - 22-10-2021
### Eliminado
- El fichero Changelog.txt.

## [v1.2] - 22-10-2021
### Añadido
- El nombre del alumno como autor en los include, los src y en Socket.cpp y Socket.h de external.

### Cambiado
- La extensión del Changelog a md.

### Eliminado
- La carpeta build del repositorio.

## [v1.1] - 15-10-2021
### Añadido
- El fichero Changelog al repositorio de Bitbucket para poder indicar los cambios realizados en las diferentes prácticas.
